// Inside of webpack.config.js:
const WorkboxPlugin = require('workbox-webpack-plugin');
const webpack = require('webpack');
const Terser = require('terser-webpack-plugin');

module.exports = {
  // Other webpack config...
  entry: './_src/index.js',
  output: {
        filename: './bundled.js',
        path: __dirname + "/public"
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production'),
    }),
    new WorkboxPlugin.InjectManifest({
      swSrc: './_src/service-worker.js',
    })
  ],
  optimization: {
    // Use terser instead of the default Uglify since service
    // worker code does not need to be transpiled to ES5.
    minimizer: [new Terser({
      // Ensure .mjs files get included.
      test: /\.m?js$/,
    })],
    // ...
  },
  // ...
};
