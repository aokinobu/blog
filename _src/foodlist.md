---
layout: page
title: Food List
permalink: /food
---

Stuff I regularly get, in no particular order:

* <a href="https://iherb.co/3dqg8xmP">Raw Honey<img src="https://s3.images-iherb.com/yso/yso12113/v/1.jpg" alt="raw honey jar" width="200"/></a>

This stuff is great.  Honey tastes complex and is sweeter than sugar.  Oh and it never spoils.  Great on bread, and also with some stinky cheese.  Not to mention pancakes or sweet rolls.  I'd like to try it for cookies.
